/*!
 *
 * Centric - Bootstrap Admin Template
 *
 * Version: 1.1
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */

// APP START
// -----------------------------------

(function() {
    'use strict';

    angular
        .module('centric', [
            'app.core',
            'app.header',
            'app.sidebar',
            'app.ripple',
            'app.menu',
            'app.preloader',
            'app.loadingbar',
            'app.translate',
            'app.settings',
            'app.utils',
            'app.listacompras',
        ]);
})();

(function() {
    'use strict';

    angular
        .module('app.colors', []);
})();

(function() {
    'use strict';

    angular
        .module('app.core', [
            'app.router',
            'ngRoute',
            'ngAnimate',
            'ngStorage',
            'ngCookies',
            'ngMessages',
            'pascalprecht.translate',
            'ui.bootstrap',
            'cfp.loadingBar',
            'ngSanitize',
            'ngResource',
            'ui.utils'
        ]);
})();

(function() {
    'use strict';

    angular
        .module('app.listacompras', []);
})();

(function() {
    'use strict';

    angular
        .module('app.header', []);
})();

(function() {
    'use strict';

    angular
        .module('app.loadingbar', []);
})();

(function() {
    'use strict';

    angular
        .module('app.menu', []);
})();

(function() {
    'use strict';

    angular
        .module('app.preloader', []);
})();

(function() {
    'use strict';

    angular
        .module('app.ripple', []);
})();

(function() {
    'use strict';

    angular
        .module('app.router', [
          'ui.router',
          'oc.lazyLoad'
        ]);
})();

(function() {
    'use strict';

    angular
        .module('app.settings', []);
})();

(function() {
    'use strict';

    angular
        .module('app.sidebar', []);
})();

(function() {
    'use strict';

    angular
        .module('app.translate', []);
})();


(function() {
    'use strict';

    angular
        .module('app.utils', [
            'app.colors'
        ]);
})();




(function() {
    'use strict';

    angular
        .module('app.colors')
        .constant('APP_COLORS', {
            'gray-darker':            '#263238',
            'gray-dark':              '#455A64',
            'gray':                   '#607D8B',
            'gray-light':             '#90A4AE',
            'gray-lighter':           '#ECEFF1',

            'primary':                '#448AFF',
            'success':                '#4CAF50',
            'info':                   '#03A9F4',
            'warning':                '#FFB300',
            'danger':                 '#F44336'
        })
        ;
})();

(function() {
    'use strict';

    angular
        .module('app.core')
        .run(colorsRun);

    colorsRun.$inject = ['$rootScope', 'Colors'];

    function colorsRun($rootScope, Colors) {

        // Allows to use branding color with interpolation
        // <tag attribute="{{ colorByName('primary') }}" />
        $rootScope.colorByName = Colors.byName;

    }

})();

(function() {
    'use strict';

    angular
        .module('app.colors')
        .service('Colors', Colors);

    Colors.$inject = ['APP_COLORS'];

    function Colors(APP_COLORS) {
        this.byName = byName;

        ////////////////

        function byName(name) {
            var color = APP_COLORS[name];
            if (!color && materialColors) {
                var c = name.split('-'); // red-500, blue-a100, deepPurple-500, etc
                if (c.length)
                    color = (materialColors[c[0]] || {})[c[1]];
            }
            return (color || '#fff');
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('app.core')
        .config(coreConfig);

    coreConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide'];

    function coreConfig($controllerProvider, $compileProvider, $filterProvider, $provide) {

        var core = angular.module('app.core');
        // registering components after bootstrap
        core.controller = $controllerProvider.register;
        core.directive = $compileProvider.directive;
        core.filter = $filterProvider.register;
        core.factory = $provide.factory;
        core.service = $provide.service;
        core.constant = $provide.constant;
        core.value = $provide.value;

    }

})();

(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('APP_MEDIAQUERY', {
            'desktopLG': 1200,
            'desktop': 992,
            'tablet': 767,
            'mobile': 480
        });

})();

(function() {
    'use strict';

    angular
        .module('app.core')
        .run(coreRoute);

    coreRoute.$inject = ['Router'];

    function coreRoute(Router) {

        Router.state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'core.layout.html',
            require: ['icons', 'ng-mfb', 'md-colors', 'screenfull']
        });
    }

})();

(function() {
    'use strict';

    angular
        .module('app.core')
        .run(coreRun);

    coreRun.$inject = ['$rootScope'];

    function coreRun($rootScope) {

        $rootScope.theme = function() {
            return $rootScope.app.theme;
        }

        $rootScope.layout = function() {
            return [

                $rootScope.sidebarVisible ? 'sidebar-visible' : '',
                $rootScope.app.sidebar.offcanvas ? 'sidebar-offcanvas' : '',
                $rootScope.sidebarOffcanvasVisible ? 'offcanvas-visible' : ''

            ].join(' ');

        }
    }

})();
(function() {
    'use strict';

    angular
        .module('app.listacompras')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', '$timeout', '$window', 'Colors', 'APP_MEDIAQUERY'];

    function DashboardController($scope, $timeout, $window, Colors, APP_MEDIAQUERY) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
            var $win = angular.element($window);

            // Main flot chart responsive
            // --------------------------
            // Checking the viewport size we can reduce the data set to display
            // less information on mobile screens with less space
            // Note: The resize technique could be expensive in some cases.

            prepareChartData();
            $win.resize(prepareChartData);

            var tm;

            function prepareChartData() {
                // don't allow multiple timers
                $timeout.cancel(tm);
                // check for mobiles
                if ($window.innerWidth < APP_MEDIAQUERY.tablet) {
                    tm = $timeout(function() {
                        vm.chartData = getChartData('mobile');
                    });
                } else {
                    tm = $timeout(function() {
                        vm.chartData = getChartData();
                    });
                }
            }

            function getChartData(target) {
                var data = [
                {
                    'label': 'Clicks',
                    'color': Colors.byName('purple-300'),
                    data: [
                        ['1', 40],
                        ['2', 50],
                        ['3', 40],
                        ['4', 50],
                        ['5', 66],
                        ['6', 66],
                        ['7', 76],
                        ['8', 96],
                        ['9', 90],
                        ['10', 105],
                        ['11', 125],
                        ['12', 135]

                    ]
                },
                {
                    'label': 'Unique',
                    'color': Colors.byName('green-400'),
                    data: [
                        ['1', 30],
                        ['2', 40],
                        ['3', 20],
                        ['4', 40],
                        ['5', 80],
                        ['6', 90],
                        ['7', 70],
                        ['8', 60],
                        ['9', 90],
                        ['10', 150],
                        ['11', 130],
                        ['12', 160]
                    ]
                }, {
                    'label': 'Recurrent',
                    'color': Colors.byName('blue-500'),
                    data: [
                        ['1', 10],
                        ['2', 20],
                        ['3', 10],
                        ['4', 20],
                        ['5', 6],
                        ['6', 10],
                        ['7', 32],
                        ['8', 26],
                        ['9', 20],
                        ['10', 35],
                        ['11', 30],
                        ['12', 56]

                    ]
                }];
                // reduce the data set when target is mobile
                if (target === 'mobile') {
                    data[0].data = data[0].data.slice(-6);
                    data[1].data = data[1].data.slice(-6);
                    data[2].data = data[2].data.slice(-6);
                }
                return data;
            }

            // first load
            vm.chartData = getChartData();
            // main chart options
            vm.chartOptions = {
                series: {
                    lines: {
                        show: false
                    },
                    points: {
                        show: false,
                        radius: 3
                    },
                    splines: {
                        show: true,
                        tension: 0.39,
                        lineWidth: 5,
                        fill: 1,
                        fillColor: Colors.byName('primary')
                    }
                },
                grid: {
                    borderColor: '#eee',
                    borderWidth: 0,
                    hoverable: true,
                    backgroundColor: 'transparent'
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, x, y) {
                        return x + ' : ' + y;
                    }
                },
                xaxis: {
                    tickColor: 'transparent',
                    mode: 'categories',
                    font: { color: Colors.byName('blueGrey-200') }
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 220, // optional: use it for a clear representation
                    tickColor: 'transparent',
                    font: { color: Colors.byName('blueGrey-200') },
                    //position: 'right' or 'left',
                    tickFormatter: function(v) {
                        return v /* + ' visitors'*/ ;
                    }
                },
                shadowSize: 0
            };

            // Bar chart stacked
            // ------------------------
            vm.stackedChartData = [{
                data: [
                    [1, 45],
                    [2, 42],
                    [3, 45],
                    [4, 43],
                    [5, 45],
                    [6, 47],
                    [7, 45],
                    [8, 42],
                    [9, 45],
                    [10, 43]
                ]
            }, {
                data: [
                    [1, 35],
                    [2, 35],
                    [3, 17],
                    [4, 29],
                    [5, 10],
                    [6, 7],
                    [7, 35],
                    [8, 35],
                    [9, 17],
                    [10, 29]
                ]
            }];
            vm.stackedChartOptions = {
                bars: {
                    show: true,
                    fill: true,
                    barWidth: 0.3,
                    lineWidth: 1,
                    align: 'center',
                    // order : 1,
                    fillColor: {
                        colors: [{
                            opacity: 1
                        }, {
                            opacity: 1
                        }]
                    }
                },
                colors: [Colors.byName('blue-100'), Colors.byName('blue-500')],
                series: {
                    shadowSize: 3
                },
                xaxis: {
                    show: true,
                    position: 'bottom',
                    ticks: 10,
                    font: { color: Colors.byName('blueGrey-200') }
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 60,
                    font: { color: Colors.byName('blueGrey-200') }
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 0,
                    color: 'rgba(120,120,120,0.5)'
                },
                tooltip: true,
                tooltipOpts: {
                    content: 'Value %x.0 is %y.0',
                    defaultTheme: false,
                    shifts: {
                        x: 0,
                        y: -20
                    }
                }
            };

            // Flot bar chart
            // ------------------
            vm.barChartOptions = {
                series: {
                    bars: {
                        show: true,
                        fill: 1,
                        barWidth: 0.2,
                        lineWidth: 0,
                        align: 'center'
                    },
                    highlightColor: 'rgba(255,255,255,0.2)'
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 0,
                    color: '#8394a9'
                },
                tooltip: true,
                tooltipOpts: {
                    content: function getTooltip(label, x, y) {
                        return 'Visitors for ' + x + ' was ' + (y * 1000);
                    }
                },
                xaxis: {
                    tickColor: 'transparent',
                    mode: 'categories',
                    font: { color: Colors.byName('blueGrey-200') }
                },
                yaxis: {
                    tickColor: 'transparent',
                    font: { color: Colors.byName('blueGrey-200') }
                },
                legend: {
                    show: false
                },
                shadowSize: 0
            };

            vm.barChartData = [{
                'label': 'New',
                bars: {
                    order: 0,
                    fillColor: Colors.byName('primary')
                },
                data: [
                    ['Jan', 20],
                    ['Feb', 15],
                    ['Mar', 25],
                    ['Apr', 30],
                    ['May', 40],
                    ['Jun', 35]
                ]
            }, {
                'label': 'Recurrent',
                bars: {
                    order: 1,
                    fillColor: Colors.byName('green-400')
                },
                data: [
                    ['Jan', 35],
                    ['Feb', 25],
                    ['Mar', 45],
                    ['Apr', 25],
                    ['May', 30],
                    ['Jun', 15]
                ]
            }];

            // Small flot chart
            // ---------------------
            vm.chartTaskData = [{
                'label': 'Total',
                color: Colors.byName('primary'),
                data: [
                    ['Jan', 14],
                    ['Feb', 14],
                    ['Mar', 12],
                    ['Apr', 16],
                    ['May', 13],
                    ['Jun', 14],
                    ['Jul', 19]
                    //4, 4, 3, 5, 3, 4, 6
                ]
            }];
            vm.chartTaskOptions = {
                series: {
                    lines: {
                        show: false
                    },
                    points: {
                        show: false
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 3,
                        fill: 1
                    },
                },
                legend: {
                    show: false
                },
                grid: {
                    show: false,
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, x, y) {
                        return x + ' : ' + y;
                    }
                },
                xaxis: {
                    tickColor: '#fcfcfc',
                    mode: 'categories'
                },
                yaxis: {
                    min: 0,
                    max: 30, // optional: use it for a clear representation
                    tickColor: '#eee',
                    //position: 'right' or 'left',
                    tickFormatter: function(v) {
                        return v /* + ' visitors'*/ ;
                    }
                },
                shadowSize: 0
            };

            // Easy Pie charts
            // -----------------

            vm.percentTask = 85;
            vm.pieOptionsTask = {
                lineWidth: 6,
                trackColor: 'transparent',
                barColor: Colors.byName('primary'),
                scaleColor: false,
                size: 90,
                lineCap: 'round',
                animate: {
                    duration: 3000,
                    enabled: true
                }
            };

            // Date picker

            vm.today = function() {
                vm.dt = new Date();
            };
            vm.today();

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);

            vm.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];

            vm.getDayClass = function(data) {
                var date = data.date,
                  mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < vm.events.length; i++) {
                        var currentDay = new Date(vm.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return vm.events[i].status;
                        }
                    }
                }

                return '';
            };

            vm.dpOptions = {
                showWeeks: false,
                customClass: vm.getDayClass
            }

            // Vector Map
            // -----------------

            // USA Map
            vm.usa_markers = [{
                latLng: [40.71, -74.00],
                name: 'New York'
            }, {
                latLng: [34.05, -118.24],
                name: 'Los Angeles'
            }, {
                latLng: [41.87, -87.62],
                name: 'Chicago',
                style: {
                    fill: Colors.byName('pink-500'),
                    r: 15
                }
            }, {
                latLng: [29.76, -95.36],
                name: 'Houston',
                style: {
                    fill: Colors.byName('purple-500'),
                    r: 10
                }
            }, {
                latLng: [39.95, -75.16],
                name: 'Philadelphia'
            }, {
                latLng: [38.90, -77.03],
                name: 'Washington'
            }, {
                latLng: [37.36, -122.03],
                name: 'Silicon Valley',
                style: {
                    fill: Colors.byName('green-500'),
                    r: 20
                }
            }];

            vm.usa_options = {
                map: 'us_mill_en',
                normalizeFunction: 'polynomial',
                backgroundColor: '#fff',
                regionsSelectable: false,
                markersSelectable: false,
                zoomButtons: false,
                zoomOnScroll: false,
                regionStyle: {
                    initial: {
                        fill: Colors.byName('blueGrey-200')
                    },
                    hover: {
                        fill: Colors.byName('gray-light'),
                        stroke: '#fff'
                    },
                },
                markerStyle: {
                    initial: {
                        fill: Colors.byName('blue-500'),
                        stroke: '#fff',
                        r: 10
                    },
                    hover: {
                        fill: Colors.byName('orange-500'),
                        stroke: '#fff'
                    }
                }
            };

            // Sparklines
            vm.sparkValue1 = [4, 4, 3, 5, 3, 4, 6, 5, 3, 2, 3, 4, 6];
            vm.sparkValue2 = [2, 3, 4, 6, 5, 4, 3, 5, 4, 3, 4, 3, 4, 5];
            vm.sparkValue3 = [4, 4, 3, 5, 3, 4, 6, 5, 3, 2, 3, 4, 6];
            vm.sparkValue4 = [6, 5, 4, 3, 5, 4, 3, 4, 3, 4, 3, 2, 2];
            vm.sparkOpts = {
                type: 'line',
                height: 20,
                width: '70',
                lineWidth: 2,
                valueSpots: {
                    '0:': Colors.byName('blue-700'),
                },
                lineColor: Colors.byName('blue-700'),
                spotColor: Colors.byName('blue-700'),
                fillColor: 'transparent',
                highlightLineColor: Colors.byName('blue-700'),
                spotRadius: 0
            };

        } //activate()
    }
})();
(function() {
    'use strict';

    angular
        .module('app.listacompras')
        .run(dashboardRun);
    dashboardRun.$inject = ['Menu'];

    function dashboardRun(Menu) {

        var menuItem = {
            name: 'Lista de Compras',
            sref: 'app.listacompras',
            // iconclass: 'ion-aperture',
            imgpath: 'static/app/img/icons/clipboard.svg',
            order: 1,
            /*label: {
                count: 2,
                classname: 'badge bg-success'
            }*/
        };

        Menu.addItem(menuItem);

    }
})();

(function() {
    'use strict';

    angular
        .module('app.listacompras')
        .run(dashboardRoute);

    dashboardRoute.$inject = ['Router'];

    function dashboardRoute(Router) {

        Router.state('app.listacompras', {
            url: '/listacompras',
            title: 'Lista de Compras',
            templateUrl: 'listacompras.html',
            require: ['ngTable','ui.select','angular-flot', 'easypiechart', 'sparkline', 'vector-map', 'vector-map-maps']
        });
    }

})();


(function() {
    'use strict';

    angular
        .module('app.listacompras')
        .controller('uiSelectController', uiSelectController);

    uiSelectController.$inject = ['$scope', '$filter', '$http', 'ngTableParams'];

    function uiSelectController($scope, $filter, $http, ngTableParams) {
        /* jshint validthis:true */
        var vm = this;

        activate();

        var data = [];

        vm.cols = [
          { field: "codigo", title: "Codigo", show: true },
          { field: "nome", title: "Nome", show: true },
          { field: "quantidade", title: "Quantidade", show: true },
          { field: "menor_preco", title: "Menor preço", show: true},
          { field: "melhor_preco_ml", title: "Menor preço dos meus locais", show: true},
        ];

        vm.mostrar = function($item,$model) {
            console.log('Selecionado', $item, $model);
        }

        vm.adicionar = function() {
            console.log('formSelected', vm.produto.selected);
            var encontrado = false;
            var i;
            for (i = 0; i < data.length; i++) {
                if(data[i].codigo == vm.produto.selected.codigo){
                    encontrado = true;
                }
            }
            if(!encontrado) {
                var selected = {
                    codigo:  vm.produto.selected.codigo,
                    nome:  vm.produto.selected.nome,
                }
                var menor_preco = 100000;
                for (i = 0; i < vm.produto.selected.precos.length; i++) {
                    var key = Object.keys(vm.produto.selected.precos[i])[0];
                    selected[key] = $filter('currency')(vm.produto.selected.precos[i][key], 'R$ ', 2);
                    if(vm.produto.selected.precos[i][key] < menor_preco) {
                        menor_preco = vm.produto.selected.precos[i][key];
                    }
                }

                selected['menor_preco'] = $filter('currency')(menor_preco, 'R$ ', 2);

                data.push(selected);
                vm.tableParams.reload();
                console.log('Nao encontrado');
            }
            console.log('data', data);
        };

        ////////////////
		vm.tableParams = new ngTableParams(
        {
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
                codigo: 'asc' // initial sorting
            }
        },
        {
            total: data.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                    $filter('orderBy')(data, params.orderBy()) :
                    data;

                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            },
        });

        function activate() {

            vm.disabled = undefined;

            vm.enable = function() {
                vm.disabled = false;
            };

            vm.disable = function() {
                vm.disabled = true;
            };

            vm.clear = function() {
                vm.produtos.selected = undefined;
            };

            vm.produto = {}; /*produto selecionado*/

            vm.runonce = false;

            vm.refreshProdutos = function(produtos) {

                /* Obtem a lista de fornecedores */
                if(!vm.runonce) {
                    $http.get(
                        '//subercompare-api.herokuapp.com/fornecedores.json'
                    ).then(function(response) {
                        vm.fornecedores = response.data;

                        console.log('Fornecedores',vm.fornecedores);
                        console.log('VM cols',vm.cols);

                        for (var i = 0; i < vm.fornecedores.length; i++) {
                            vm.cols.push(
                                {
                                    field:  vm.fornecedores[i].id,
                                    title:  vm.fornecedores[i].nome,
                                    show:   true,
                                }
                            )
                        }
                    });
                    vm.runonce = true;
                }

                return $http.get(
                    '//subercompare-api.herokuapp.com/cotacoes.json'
                ).then(function(response) {
                    vm.produtos = response.data;
                });
            };
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.header')
        .controller('HeaderController', HeaderController)
        .controller('HeaderModalController', HeaderModalController)
        .controller('HeaderModalSearchController', HeaderModalSearchController);

    HeaderController.$inject = ['$uibModal'];

    function HeaderController($uibModal) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
            // Header Search
            vm.openModalSearch = function() {

                var modalSearchInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'static/app/views/header-search.tpl.html',
                    controller: 'HeaderModalSearchController as mod',
                    // position via css class
                    windowClass: 'modal-top',
                    backdropClass: 'modal-backdrop-soft',
                    // sent data to the modal instance (injectable into controller)
                    resolve: {
                        data: function() {
                            return {
                                title: 'Search'
                            };
                        }
                    }
                });

                modalSearchInstance.result.then(function( /*data*/ ) {
                    // use data from modal here
                }, function() {
                    // Modal dismissed
                });
            };

            // Settings panel (right sidebar)
            vm.openModalBar = function() {

                var modalBarInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'static/app/views/settings.tpl.html',
                    controller: 'HeaderModalController as mod',
                    // position via css class
                    windowClass: 'modal-right',
                    backdropClass: 'modal-backdrop-soft',
                    // sent data to the modal instance (injectable into controller)
                    resolve: {
                        data: function() {
                            return {
                                title: 'Settings'
                            };
                        }
                    }
                });

                modalBarInstance.result.then(function( /*data*/ ) {
                    // use data from modal here
                }, function() {
                    // Modal dismissed
                });
            };

        }
    }

    HeaderModalController.$inject = ['$uibModalInstance', 'data'];

    function HeaderModalController($uibModalInstance, data) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

            vm.modalTitle = data.title;

            vm.close = function() {
                $uibModalInstance.close( /* data for promise*/ );
            };

            vm.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
        }
    }
    HeaderModalSearchController.$inject = ['$uibModalInstance', '$timeout', 'data'];

    function HeaderModalSearchController($uibModalInstance, $timeout, data) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

            vm.modalTitle = data.title;

            // input autofocus
            $timeout(function() {
                document.querySelector('.header-input-search').focus();
            }, 300);

            vm.close = function() {
                $uibModalInstance.close( /* data for promise*/ );
            };

            vm.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
        }
    }

})();

(function() {
    'use strict';

    // This component is only used to provide a link in the menu
    // to the jQuery demo. It shows the menu support for direct
    // links using 'href' property.
    angular
        .module('centric')
        .run(jQueryDemoRun);
    jQueryDemoRun.$inject = ['Menu'];

    function jQueryDemoRun(Menu) {

        var menuItem = {
            name: 'HTML5/jQuery',
            href: '../html5jquery/',
            iconclass: 'ion-android-open',
            order: 99
        };

        // Menu.addItem(menuItem);

    }
})();


(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .config(loadingbarConfig);
    loadingbarConfig.$inject = ['cfpLoadingBarProvider'];

    function loadingbarConfig(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = true;
        cfpLoadingBarProvider.latencyThreshold = 500;
        //cfpLoadingBarProvider.parentSelector = '';
    }
})();

(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .run(loadingbarRun);
    loadingbarRun.$inject = ['$rootScope', '$timeout', 'cfpLoadingBar'];

    function loadingbarRun($rootScope, $timeout, cfpLoadingBar) {

        // Loading bar transition
        // -----------------------------------
        var thBar;
        $rootScope.$on('$stateChangeStart', function() {
            thBar = $timeout(function() {
                cfpLoadingBar.start();
            }, 0); // sets a latency Threshold
        });
        $rootScope.$on('$stateChangeSuccess', function(event) {
            event.targetScope.$watch('$viewContentLoaded', function() {
                $timeout.cancel(thBar);
                cfpLoadingBar.complete();
            });
        });

    }

})();

(function() {
    'use strict';

    angular
        .module('app.menu')
        .controller('MenuController', MenuController);

    MenuController.$inject = ['Menu'];

    function MenuController(Menu) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
            vm.items = Menu.getItems();
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.menu')
        .service('Menu', Menu);

    function Menu() {
        /* jshint validthis:true */
        this.addItem = addItem;
        this.getItems = getItems;

        ////////////////

        this.menu = [];

        function addItem(item) {
            validate(item);
            this.menu.push(item);
        }

        function getItems() {
            return this.menu;
        }

        // validate items and throw error when can't recover
        function validate(item) {
            if (!angular.isDefined(item))
                throw new Error('Menu item not defined.');
            if (!angular.isDefined(item.name))
                throw new Error('Menu item name not defined.');
            if (!angular.isDefined(item.order))
                item.order = 0; // order must exists
            // item ok
            return item;
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('app.preloader')
        .directive('preloader', preloader);

    preloader.$inject = ['$animate', '$timeout', '$q'];

    function preloader($animate, $timeout, $q) {

        var directive = {
            restrict: 'EAC',
            template: '<div class="preloader-desc">Carregando SuberCompare...</div>' +
                '<div class="preloader-progress">' +
                '<div class="preloader-progress-bar" ' +
                'ng-style="{width: loadCounter + \'%\'}"></div>' +
                '</div>',
            link: link
        };
        return directive;

        ///////

        function link(scope, el) {

            scope.loadCounter = 0;

            var counter = 0,
                timeout;

            // disables scrollbar
            angular.element('body').css('overflow', 'hidden');
            // ensure class is present for styling
            el.addClass('preloader');

            appReady().then(function() {
                $timeout(endCounter, 500);
            });

            timeout = $timeout(startCounter);

            ///////

            function startCounter() {

                var remaining = 100 - counter;
                counter = counter + (0.0175 * Math.pow(1 - Math.sqrt(remaining), 2));

                scope.loadCounter = parseInt(counter, 10);

                timeout = $timeout(startCounter, 20);
            }

            function endCounter() {

                $timeout.cancel(timeout);

                scope.loadCounter = 100;

                $timeout(function() {
                    // animate preloader hiding
                    $animate.addClass(el, 'preloader-hidden');
                    // retore scrollbar
                    angular.element('body').css('overflow', '');
                }, 300);
            }

            function appReady() {
                var deferred = $q.defer();
                var fired = 0;
                // if this doesn't sync with the real app ready
                // a custom event must be used instead
                var off = scope.$on('$viewContentLoaded', function() {
                    fired++;
                    // Wait for two events since we have two main ui-view
                    if ( fired > 1 ) {
                        deferred.resolve();
                        off();
                    }
                });

                return deferred.promise;
            }

        } //link
    }

})();

(function() {
    'use strict';

    angular
        .module('app.ripple')
        .directive('ripple', ripple);

    function ripple() {

        return {
            restrict: 'C',
            link: link
        };

        function link(scope, element) {
            new Ripple(element);
        }

    }

})();

(function(global) {
    'use strict';

    // public interface
    global.Ripple = RippleEffect;

    /**
     * Ripple effect for common components
     * @param [element] jQuery or jqLite element
     */
    function RippleEffect(element) {
        var TRANSITION_END = 'transitionend webkitTransitionEnd';
        var jq = angular.element;

        this.element = element;
        this.rippleElement = this.getElement();
        this.$rippleElement = jq(this.rippleElement);

        var clickEv = this.detectClickEvent();

        var self = this;
        element.on(clickEv, function() {
            // remove animation on click
            self.$rippleElement.removeClass('md-ripple-animate');
            // Set ripple size and position
            self.calcSizeAndPos();
            // start to animate
            self.$rippleElement
                .addClass('md-ripple-animate');
        });

        this.$rippleElement.on(TRANSITION_END, function() {
            self.$rippleElement
                .removeClass('md-ripple-animate');
            // avoid weird affect when ripple is not active
            self.rippleElement.style.width = 0;
            self.rippleElement.style.height = 0;
        });
    }
    /**
     * Returns the elements used to generate the effect
     * If not exists, it is created by appending a new
     * dom element
     */
    RippleEffect.prototype.getElement = function() {
        var dom = this.element[0];
        var rippleElement = dom.querySelector('.md-ripple');

        if (rippleElement === null) {
            // Create ripple
            rippleElement = document.createElement('span');
            rippleElement.className = 'md-ripple';
            // Add ripple to element
            this.element.append(rippleElement);
        }
        return rippleElement;
    };

    /**
     * Determines the better size for the ripple element
     * based on the element attached and calculates the
     * position be fully centered
     */
    RippleEffect.prototype.calcSizeAndPos = function() {
        var size = Math.max(this.element.width(), this.element.height());
        this.rippleElement.style.width = size + 'px';
        this.rippleElement.style.height = size + 'px';
        // autocenter (requires css)
        this.rippleElement.style.marginTop = -(size / 2) + 'px';
        this.rippleElement.style.marginLeft = -(size / 2) + 'px';
    };

    RippleEffect.prototype.detectClickEvent = function() {
        var isIOS = ((/iphone|ipad/gi).test(navigator.appVersion));
        return isIOS ? 'touchstart' : 'click';
    };

})(window);

(function() {
    'use strict';

    angular
        .module('app.router')
        .config(routerConfig);

    routerConfig.$inject = ['$ocLazyLoadProvider', 'APP_REQUIRES'];

    function routerConfig($ocLazyLoadProvider, APP_REQUIRES) {

        // Lazy Load modules configuration
        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: APP_REQUIRES.modules
        });

    }
})();

(function() {
    'use strict';

    angular
        .module('app.router')
        .constant('APP_REQUIRES', {
            'modernizr': {
                files: ['static/vendor/modernizr/modernizr.custom.js']
            },
            'icons': {
                files: ['static/vendor/ionicons/css/ionicons.min.css']
            },
            'fontawesome': {
                files: ['static/vendor/font-awesome/css/font-awesome.min.css']
            },
            'screenfull': {
                files: ['static/vendor/screenfull/dist/screenfull.js']
            },
            'lodash': {
                files: ['static/vendor/lodash/dist/lodash.min.js']
            },
            'md-colors': {
                files: ['static/vendor/material-colors/dist/colors.css']
            },
            'sparkline': {
                files: ['static/vendor/sparkline/index.js']
            },
            'ng-mfb': {
                files: ['static/vendor/ng-mfb/mfb/dist/mfb.min.css',
                    'static/vendor/ng-mfb/src/mfb-directive.js'
                ]
            },
            'easypiechart': {
                files: ['static/vendor/jquery.easy-pie-chart/dist/angular.easypiechart.min.js']
            },
            'angular-flot': {
                'serie': true,
                files: ['static/vendor/flot/jquery.flot.js',
                    'static/vendor/flot/jquery.flot.categories.js',
                    'static/vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
                    'static/vendor/flot/jquery.flot.resize.js',
                    'static/vendor/flot/jquery.flot.pie.js',
                    'static/vendor/flot/jquery.flot.time.js',
                    'static/vendor/sidebysideimproved/jquery.flot.orderBars.js',
                    'static/vendor/flot-spline/js/jquery.flot.spline.min.js',
                    'static/vendor/angular-flot/angular-flot.js'
                ]
            },
            'ui.select': {
                files: ['static/vendor/angular-ui-select/dist/select.js',
                    'static/vendor/angular-ui-select/dist/select.css'
                ]
            },
            'uiGmapgoogle-maps': {
                files: [
                    'static/vendor/angular-simple-logger/dist/angular-simple-logger.min.js',
                    'static/vendor/angular-google-maps/dist/angular-google-maps.min.js'
                ]
            },
            'angular-rickshaw': {
                serie: true,
                files: ['static/vendor/d3/d3.min.js',
                    'static/vendor/rickshaw/rickshaw.js',
                    'static/vendor/rickshaw/rickshaw.min.css',
                    'static/vendor/angular-rickshaw/rickshaw.js'
                ]
            },
            'ui.knob': {
                files: ['static/vendor/angular-knob/src/angular-knob.js',
                    'static/vendor/jquery-knob/dist/jquery.knob.min.js'
                ]
            },
            'oitozero.ngSweetAlert': {
                files: ['static/vendor/sweetalert/dist/sweetalert.css',
                    'static/vendor/sweetalert/dist/sweetalert.min.js',
                    'static/vendor/angular-sweetalert/SweetAlert.js'
                ]
            },
            'the-cormoran.angular-loaders': {
                files: [
                    'static/vendor/loaders.css/loaders.css',
                    'static/vendor/angular-loaders/angular-loaders.js'
                ]
            },
            'angularBootstrapNavTree': {
                files: ['static/vendor/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
                    'static/vendor/angular-bootstrap-nav-tree/dist/abn_tree.css'
                ]
            },
            'ng-nestable': {
                files: ['static/vendor/ng-nestable/src/angular-nestable.js',
                    'static/vendor/nestable/jquery.nestable.js'
                ]
            },
            'akoenig.deckgrid': {
                files: ['static/vendor/angular-deckgrid/angular-deckgrid.js']
            },
            'vr.directives.slider': {
                files: ['static/vendor/venturocket-angular-slider/build/angular-slider.min.js']
            },
            'xeditable': {
                files: ['static/vendor/angular-xeditable/dist/js/xeditable.js',
                    'static/vendor/angular-xeditable/dist/css/xeditable.css'
                ]
            },
            'colorpicker.module': {
                files: ['static/vendor/angular-bootstrap-colorpicker/css/colorpicker.css',
                    'static/vendor/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js'
                ]
            },
            'summernote': {
                serie: true,
                insertBefore: '#appcss',
                files: [
                    'static/vendor/bootstrap/js/modal.js',
                    'static/vendor/bootstrap/js/dropdown.js',
                    'static/vendor/bootstrap/js/tooltip.js',
                    'static/vendor/summernote/dist/summernote.css',
                    'static/vendor/summernote/dist/summernote.js',
                    'static/vendor/angular-summernote/dist/angular-summernote.js'
                ]
            },
            'angularFileUpload': {
                files: ['static/vendor/angular-file-upload/dist/angular-file-upload.min.js']
            },
            'filestyle': {
                files: ['static/vendor/bootstrap-filestyle/src/bootstrap-filestyle.js']
            },
            'ngDropzone': {
                serie: true,
                insertBefore: '#appcss',
                files: [
                    'static/vendor/dropzone/dist/basic.css',
                    'static/vendor/dropzone/dist/dropzone.css',
                    'static/vendor/dropzone/dist/dropzone.js',
                    'static/vendor/angular-dropzone/lib/angular-dropzone.js'
                ]
            },
            'vector-map': {
                files: ['static/vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js',
                    'static/vendor/ika.jvectormap/jquery-jvectormap-1.2.2.css'
                ]
            },
            'vector-map-maps': {
                files: ['static/vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js',
                    'static/vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js'
                ]
            },
            'datatables': {
                serie: true,
                files: ['static/vendor/datatables/media/css/jquery.dataTables.css',
                    'static/vendor/datatables/media/js/jquery.dataTables.js',
                    'static/vendor/angular-datatables/dist/angular-datatables.js',
                    'static/vendor/angular-datatables/dist/plugins/bootstrap/datatables.bootstrap.css',
                    'static/vendor/angular-datatables/dist/plugins/bootstrap/angular-datatables.bootstrap.js'
                ]
            },
            'ngTable': {
                files: ['static/vendor/ng-table/dist/ng-table.min.js',
                    'static/vendor/ng-table/dist/ng-table.min.css'
                ]
            },
            'ngTableExport': {
                files: ['static/vendor/ng-table-export/ng-table-export.js']
            },
            'blueimp-gallery': {
                files: ['static/vendor/blueimp-gallery/js/jquery.blueimp-gallery.min.js',
                    'vendor/blueimp-gallery/css/blueimp-gallery.min.css'
                ]
            }
        });

})();
(function() {
    'use strict';

    angular
        .module('app.router')
        .provider('Router', RouterProvider);

    RouterProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];

    function RouterProvider($locationProvider, $stateProvider, $urlRouterProvider) {

        var config = {
            // The paths where html template resides
            viewsBasePath: 'static/app/views/',
            // Automatically prepend views path to all templatesUrl?
            useViewsBasePath: true,
            // Set the following to true to enable the HTML5 Mode
            // You may have to set <base> tag in index and a routing configuration in your server
            html5Mode: false,
            // defaults to dashboard
            defaultRoute: '/app/listacompras'
        };

        // public access to change configuration
        this.configure = function(cfg) {
            angular.extend(config, cfg);
        };

        $locationProvider.html5Mode(config.html5Mode);

        $urlRouterProvider.otherwise(config.defaultRoute);

        this.$get = Router;

        Router.$inject = ['$rootScope', '$state', '$stateParams', 'APP_REQUIRES'];

        function Router($rootScope, $state, $stateParams, APP_REQUIRES) {
            /* jshint validthis:true */

            var service = {
                // service access level
                viewpath: viewpath,
                resolveFor: resolveFor,
                state: state,
                getStates: getStates
            };

            init();

            return service;

            ///////

            // wrapper for $stateProvider to simply routes creation
            function state(name, options) {
                if (!name) throw new Error('Route name not defined.');

                if (options.require) {
                    var require = this.resolveFor.apply(this, options.require);
                    options.resolve = angular.extend({}, options.resolve, require);
                }
                if (options.templateUrl && config.useViewsBasePath)
                    options.templateUrl = this.viewpath(options.templateUrl);

                $stateProvider.state(name, options);

                // allow chain execution
                return this;
            }

            // Set here the base of the
            // relative path for all views
            function viewpath(uri) {
                return config.viewsBasePath + uri;
            }

            // Generates a resolve object by passing script names
            // previously configured in constant.APP_REQUIRES
            function resolveFor() {
                var _args = arguments;
                return {
                    __deps: ['$ocLazyLoad', '$q', function($ocLL, $q) {
                        // Creates a promise chain for each argument
                        var promiseChain = $q.when(1); // empty promise
                        for (var i = 0, len = _args.length; i < len; i++) {
                            promiseChain = andThen(_args[i]);
                        }
                        return promiseChain;

                        // creates promise to chain dynamically
                        function andThen(mod) {
                            // support a function that returns a promise
                            if (typeof mod === 'function')
                                return promiseChain.then(mod);
                            else {
                                return promiseChain.then(function() {
                                    // check if module is defined
                                    if (!APP_REQUIRES[mod])
                                        throw new Error('Route resolve: Bad resource name [' + mod + ']');
                                    // finally, return the load promise
                                    return $ocLL.load(APP_REQUIRES[mod]);
                                });
                            }
                        }

                    }]
                };
            } // resolveFor

            function getStates() {
                return $state.get();
            }

            function init() {

                // Set reference to access them from any scope
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;

                // auto update document title
                $rootScope.$on('$stateChangeSuccess',
                    function(event, toState /*, toParams, fromState, fromParams*/ ) {
                        // Autoscroll to top
                        scrollTopMainView();
                        // Update document title
                        var title = (toState.title || '');
                        $rootScope.documentTitle = title; // data bind to <title>
                    }
                );
                // on state not found log to console
                $rootScope.$on('$stateNotFound',
                    function(event, unfoundState /*, fromState, fromParams*/ ) {
                        console.log('State not found: ' + unfoundState.to + unfoundState.toParams + unfoundState.options);
                    });
                // on error log to console
                $rootScope.$on('$stateChangeError',
                    function(event, toState, toParams, fromState, fromParams, error) {
                        console.log(error);
                    });
            }

            function scrollTopMainView() {
                // There must not be more than one <main> element in a document. (http://www.w3schools.com/tags/tag_main.asp)
                var main = document.querySelector('main');
                if(main) main.scrollTop = 0;
            }
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.settings')
        .run(settingsRun);

    settingsRun.$inject = ['$rootScope'];

    function settingsRun($rootScope) {

        var themes = [
            'theme-1',
            'theme-2',
            'theme-3',
            'theme-4',
            'theme-5',
            'theme-6',
            'theme-7',
            'theme-8',
            'theme-9',
        ]

        // Global Settings
        // -----------------------------------
        $rootScope.app = {
            name: 'Centric',
            description: 'Bootstrap Admin Template',
            year: ((new Date()).getFullYear()),
            layout: {
                rtl: false
            },
            sidebar: {
                over: false,
                showheader: false,
                showtoolbar: true,
                offcanvas: true
            },
            header: {
                menulink: 'menu-link-slide'
            },
            footerHidden: false,
            viewAnimation: 'ng-fadeInLeftShort',
            theme: themes[0],
            currentTheme: 0
        };

        $rootScope.themes = themes;

    }

})();
(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .directive('sidebarNav', sidebarNav);

    sidebarNav.$inject = [];

    function sidebarNav() {
        return {
            restrict: 'EAC',
            link: link
        };

        function link(scope, element) {

            element.on('click', function(event) {
                var item = getItemElement(event);
                // check click is on a tag
                if (!item) return;

                var ele = angular.element(item),
                    liparent = ele.parent()[0];

                var lis = ele.parent().parent().children(); // markup: ul > li > a
                // remove .active from childs
                lis.find('li').removeClass('active');
                // remove .active from siblings ()
                angular.forEach(lis, function(li) {
                    if (li !== liparent)
                        angular.element(li).removeClass('active');
                });

                var next = ele.next();
                if (next.length && next[0].tagName === 'UL') {
                    ele.parent().toggleClass('active');
                    event.preventDefault();
                }
            });

        }

        // find the a element in click context
        // doesn't check deeply, asumens two levels only
        function getItemElement(event) {
            var element = event.target,
                parent = element.parentNode;
            if (element.tagName.toLowerCase() === 'a') return element;
            if (parent.tagName.toLowerCase() === 'a') return parent;
            if (parent.parentNode.tagName.toLowerCase() === 'a') return parent.parentNode;
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .run(sidebarRun);
    sidebarRun.$inject = ['$rootScope', '$window', '$document', '$timeout', 'APP_MEDIAQUERY'];

    function sidebarRun($rootScope, $window, $document, $timeout, APP_MEDIAQUERY) {
        // Sidebar API for mobile
        $rootScope.toggleSidebar = toggleSidebarState;
        $rootScope.closeSidebar = function() {
            toggleSidebarState(false);
        };
        $rootScope.openSidebar = function() {
            toggleSidebarState(true);
        };

        // Sidebar offcanvas API for desktops
        $rootScope.toggleSidebarOffcanvasVisible = function(state) {
            $rootScope.sidebarOffcanvasVisible = angular.isDefined(state) ? state : !$rootScope.sidebarOffcanvasVisible;
        };

        // ESC key close sidebar
        $document.on('keyup',function(e) {
             if (e.keyCode == 27) {
                $timeout(function() {
                    $rootScope.toggleSidebarOffcanvasVisible(false);
                });
            }
        });

        // Considerations for different APP states

        // on mobiles, sidebar starts off-screen
        if (isMobileScreen()) $timeout(function() {
            toggleSidebarState(false);
        });

        // hide sidebar when open a new view
        $rootScope.$on('$stateChangeStart', function() {
            if (isMobileScreen())
                toggleSidebarState(false);
            // Always hide offscreen sidebar when route change
            else
                $rootScope.toggleSidebarOffcanvasVisible(false);
        });

        // remove desktop offcanvas when app changes to mobile
        // so when it returns, the sidebar is shown again
        $window.addEventListener('resize', function() {
            if (isMobileScreen())
                $rootScope.toggleSidebarOffcanvasVisible(false);
        });

        ///////

        function toggleSidebarState(state) {
            //  state === true -> open
            //  state === false -> close
            //  state === undefined -> toggle
            $rootScope.sidebarVisible = angular.isDefined(state) ? state : !$rootScope.sidebarVisible;
        }

        function isMobileScreen() {
            return $window.innerWidth < APP_MEDIAQUERY.desktop;
        }
    }
})();


(function() {
    'use strict';

    angular
        .module('app.translate')
        .config(translateConfig);
    translateConfig.$inject = ['$translateProvider'];

    function translateConfig($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix: 'static/server/i18n/',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('pt_BR');
        $translateProvider.useLocalStorage();
        $translateProvider.usePostCompiling(true);
        $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

    }
})();

(function() {
    'use strict';

    angular
        .module('app.translate')
        .run(translateRun);
    translateRun.$inject = ['$rootScope', '$translate'];

    function translateRun($rootScope, $translate) {

        // Internationalization
        // ----------------------

        $rootScope.language = {
            // Handles language dropdown
            listIsOpen: false,
            // list of available languages
            available: {
                'en': 'English',
                'es_AR': 'Español',
                'pt_BR': 'Português Brasileiro'
            },
            // display always the current ui language
            init: function() {
                var proposedLanguage = $translate.proposedLanguage() || $translate.use();
                var preferredLanguage = $translate.preferredLanguage(); // we know we have set a preferred one in app.config
                $rootScope.language.selected = $rootScope.language.available[(proposedLanguage || preferredLanguage)];
            },
            set: function(localeId) {
                // Set the new idiom
                $translate.use(localeId);
                // save a reference for the current language
                $rootScope.language.selected = $rootScope.language.available[localeId];
                // finally toggle dropdown
                $rootScope.language.listIsOpen = !$rootScope.language.listIsOpen;
            }
        };

        $rootScope.language.init();

    }
})();

(function() {
    'use strict';
    angular
        .module('app.utils')
        .service('Browser', Browser);

    Browser.$inject = ['$window'];

    // Browser detection
    function Browser($window) {
        return $window.jQBrowser;
    }

})();
(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('toggleFullscreen', toggleFullscreen);

    toggleFullscreen.$inject = ['Browser'];

    function toggleFullscreen(Browser) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
            // Not supported under IE
            if (Browser.msie) {
                element.addClass('hide');
            } else {
                element.on('click', function(e) {
                    e.preventDefault();

                    if (screenfull.enabled) {

                        screenfull.toggle();

                    } else {
                        // Fullscreen not enabled ;
                    }

                });
            }
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('svgReplace', svgReplace);

    svgReplace.$inject = ['$compile', '$http', '$templateCache', '$timeout']
    function svgReplace ($compile, $http, $templateCache, $timeout) {

        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            $timeout(function() {

                var src = attrs.src;

                if( !src || src.indexOf('.svg') < 0)
                    throw "only support for SVG images";
                    // return /*only support for SVG images*/;

                $http.get(src, {
                    cache : $templateCache
                }).success(function (res) {
                    element.replaceWith($compile(res)(scope))
                })

            });
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('triggerResize', triggerResize);

    triggerResize.$inject = ['$window', '$timeout'];

    function triggerResize($window, $timeout) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
            element.on('click', function() {
                $timeout(function() {
                    $window.dispatchEvent(new Event('resize'));
                });
            });
        }
    }

})();
