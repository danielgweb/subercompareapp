# -*- coding: utf-8 -*-

"""
    URLs do app
"""

from django.conf.urls import url
from app.views import AppView

urlpatterns = [
    url(r'^$', AppView.as_view(), name='app')
]
